Go Data Analysis package
========================

*GoDA is designed for generic particle physics style data analyses.*


Usage
-----


First get goDA:

````
go get bitbucket.org/NickRyder/goDA/data
````

To write a data analysis using goDA you should import the package and write a number of structures matching the `data.Algorithm` interface:

````
type Algorithm interface {
	Initialise(* Analysis) error
	Execute() (bool, error)
	Finalise() error
	Name() string
	Requires() []string
}
````

An analysis can be performed with a main function that uses a `data.Analysis` object, adding a number of structs matching the `data.Algorithm` interface.
As each `data.Algorithm` is added its `Algorithm.Initialise` method is called.
When the `data.Analysis.Run()` is called 

At least on `data.Algorithm` should be written to read the input data, make it available to other algorithms and return `finished = true` when the data source has been exhausted.
Each `data.Algorithm` may access other algorithms that have previously been added to the analysis using the `Analysis.Get(name string)` method.
This should be called in the algorithm's `Initialise()` method, and the returned `data.Algorithm` should be converted to a concrete type:

````
type AlgoX struct {
    y AlogY
}

func (x AlgoX) Initialise(analysis *data.Analysis) error {
    yalgo, err := analysis.Get("YYY")
    if err != nil {
        return err
    }
    y, ok := yalgo.(AlogY)
    if !ok {
        return fmt.Errorf("AlgoX.Initialise: type assertion for AlgoY failed.")
    }
    x.y := y
    // Other initilisation code
}

func (x AlgoX) Execute() (keep, finished bool, err error) {
    keep = true
    z := x.y.Z()
    // Do something with z
}

````

If any of the added algorithms returns an error during initialisation the analysis will not loop over any data, but will try to `Finalise()` all algorithms.
Each algorithm should flag that the current event should not be further processed by returning `keep = false` or that no more events should be analysed by returning `finished = true`.
At the end of the event analysis phase the `Analyser.Run()` will call each algorithm's `Finalise()` method unless or until on returns a non-nil error.
