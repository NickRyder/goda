package data

import (
	"fmt"
)

type Algorithm interface {
	Initialise(* Analysis) error
	Execute() (keep, finished bool, err error)
	Finalise() error
	Name() string
	Requires() []string
}

type Analysis struct {
	algorithms []Algorithm
	algoindices map[string]int
}

func (a Analysis) Run() error {
	finished := false
	for !finished {
		for _, algo := range a.algorithms {
			keep, finished, err := algo.Execute()
			if err != nil {
				return err
			}
			if !keep || finished {
				break
			}
		}
	}
	for _, algo := range a.algorithms {
		if err := algo.Finalise(); err != nil {
			return err
		}
	}
	return nil
}

func (a * Analysis) Add(algo Algorithm) error {
	if err := algo.Initialise(a); err != nil {
		return err
	}
	name := algo.Name()
	if _, ok := a.algoindices[name]; ok {
		return fmt.Errorf("Analysis.Add() '%s' already exists", name)
	}
	reqs := algo.Requires()
	missing := make([]string, 0, len(reqs))
	for _, req := range reqs {
		if _, ok := a.algoindices[req]; !ok {
			missing = append(missing, req)
		}
	}
	if len(missing) > 0 {
		return fmt.Errorf("Analysis.Add(): '%s' missing dependencies: %v", name, missing)
	}
	a.algoindices[name] = len(a.algorithms)
	a.algorithms = append(a.algorithms, algo)
	return nil
}

func (a * Analysis) Get(name string) (Algorithm, error) {
	i, ok := a.algoindices[name]
	if !ok {
		return nil, fmt.Errorf("Analysis.Get() unknown algorithm: %s", name)
	}
	return a.algorithms[i], nil
}

type FakeAlgorithm struct {
}

var fakeerr = fmt.Errorf("Fake algorithm!")
func (f FakeAlgorithm) Initialise(analysis *Analysis) error {
	return fakeerr
}

func (f FakeAlgorithm) Execute() (bool, error) {
	return false, fakeerr
}

func (f FakeAlgorithm) Finalise(analysis *Analysis) error {
	return fakeerr
}

func (f FakeAlgorithm) Name() string {
	return "fake"
}

func (f FakeAlgorithm) Requires() []string {
	return []string{"nonexistent"}
}
